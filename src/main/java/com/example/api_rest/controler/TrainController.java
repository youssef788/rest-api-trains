package com.example.api_rest.controler;

import com.example.api_rest.model.Reservation;
import com.example.api_rest.model.ReservationRequestBean;
import com.example.api_rest.model.Ticket;
import com.example.api_rest.model.Train;
import com.example.api_rest.service.ITrainService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TrainController {
    private ITrainService trainService;

    public TrainController (ITrainService service){
        this.trainService = service;
    }

    @GetMapping("/allTrains")
    @Operation(summary = "List of all trains")
    public List<Train> getAllTrains() {
        return trainService.getAllTrains();
    }

    @PostMapping(value ="/availableTrains", consumes = "application/json", produces = "application/json")
    @Operation(summary = "List of all available trains filtering by train")
    public List<Train> getAvailableTrains(@RequestBody Train train) throws Exception {
        return trainService.getAvailableTrains(train.getStationDeparture(), train.getStationArrival(), train.getDateDeparture(),
                train.getDateArrival(), train.getNbPlacesAvailable());
    }

    @PostMapping(value ="/createReservation", consumes = "application/json", produces = "application/json")
    @Operation(summary = "Create an reservation with departure and eventually return")
    public ResponseEntity<Reservation> createReservation(@RequestBody ReservationRequestBean requestBean) {
        Reservation reservation = new Reservation();
        List<Ticket> ticketsAller = trainService.getCorrespondingTickets(requestBean.getIdTrain(), requestBean.getSeatClass(), requestBean.getFlexible());

        if(ticketsAller == null)
            return ResponseEntity.badRequest().build();

        Ticket ticketAller = ticketsAller.get(0);

        reservation.addTicket(ticketAller);
        trainService.removeTicketFromTrain(ticketAller.getId());

        if(isRoundTrip(requestBean)){
            List<Ticket> ticketsReturn = trainService.getCorrespondingTickets(requestBean.getIdTrainReturn(), requestBean.getSeatClassReturn(), requestBean.getFlexibleReturn());

            if(ticketsReturn == null)
                return ResponseEntity.badRequest().build();

            Ticket ticketReturn = ticketsReturn.get(0);
            reservation.addTicket(ticketReturn);
            trainService.removeTicketFromTrain(ticketReturn.getId());
        }

        return ResponseEntity.ok(reservation);
    }

    private boolean isRoundTrip(ReservationRequestBean requestBean){
        return (requestBean.getIdTrainReturn()!=null && requestBean.getSeatClassReturn()!=null && requestBean.getFlexibleReturn()!=null);
    }

}
