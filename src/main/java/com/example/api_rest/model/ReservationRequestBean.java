package com.example.api_rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Optional;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class ReservationRequestBean {

    private Optional<Integer> idTrain;
    private Optional<String>  seatClass;
    private Optional<Boolean>  flexible;
    private Optional<Integer> idTrainReturn;
    private Optional<String> seatClassReturn;
    private Optional<Boolean> flexibleReturn;

}
