package com.example.api_rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name="train")
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Train implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String line;
    @Column(name = "station_departure")
    private String stationDeparture;
    @Column(name = "station_arrival")
    private String stationArrival;
    @Column(name = "date_departure")
    private Date dateDeparture;
    @Column(name = "date_arrival")
    private Date dateArrival;
    @Column(name = "nb_places_available")
    private Integer nbPlacesAvailable;
    @OneToMany(mappedBy="idTrain", cascade = CascadeType.REMOVE)
    @OrderBy("idTrain ASC")
    private List<Ticket> tickets;
}

