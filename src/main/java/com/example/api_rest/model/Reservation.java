package com.example.api_rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class Reservation {
    private int id;
    private int idUser;
    private List<Ticket> tickets;

    public Reservation(){
        this.id = 0;
        this.idUser = 0;
        this.tickets = new ArrayList<>();
    }

    public void addAllTickets(List<Ticket> tickets) {
        this.tickets.addAll(tickets);
    }
    public void addTicket(Ticket ticket) {

        this.tickets.add(ticket);
    }
}
