package com.example.api_rest.service;

import com.example.api_rest.model.Ticket;
import com.example.api_rest.model.Train;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ITrainService {
    List<Train> getAllTrains();

    List<Train> getAvailableTrains(String stationDeparture, String stationArrival,
                                   Date dateDeparture, Date dateArrival,
                                   Integer nbPlacesAvailable) throws Exception;
    List<Ticket> getCorrespondingTickets(Optional<Integer> idTrain, Optional<String> seatClass, Optional<Boolean> flexible);

    @Transactional
    void removeTicketFromTrain(int idTicket);
}
