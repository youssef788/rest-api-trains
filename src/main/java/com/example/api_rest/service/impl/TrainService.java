package com.example.api_rest.service.impl;

import com.example.api_rest.dao.ITrainDao;
import com.example.api_rest.model.Ticket;
import com.example.api_rest.model.Train;
import com.example.api_rest.service.ITrainService;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TrainService implements ITrainService {
    private ITrainDao trainDao;

    public TrainService(ITrainDao dao) {
        this.trainDao = dao;
    }

    public List<Train> getAllTrains() {
        return trainDao.findAll();
    }

    public List<Train> getAvailableTrains(String stationDeparture, String stationArrival,
                                          Date dateDeparture, Date dateArrival,
                                          Integer nbPlacesAvailable) throws Exception {

        List<Train> availableTrains = trainDao.findAvailableTrains(stationDeparture, stationArrival, dateDeparture,
                dateArrival, nbPlacesAvailable);
        if (availableTrains != null && !availableTrains.isEmpty()){
            return availableTrains;
        }
        else {
            return Collections.emptyList();
        }
    }
    
    
   public List<Ticket> getCorrespondingTickets(Optional<Integer> idTrain, Optional<String> seatClass, Optional<Boolean> flexible){
        List<Ticket> correspondingTickets = trainDao.findCorrespondingTickets(idTrain.get(), seatClass.get(), flexible.get());
       if (correspondingTickets != null && !correspondingTickets.isEmpty()){
           return correspondingTickets;
       } else {
           return Collections.emptyList();
       }
   }

   @Transactional
   public void removeTicketFromTrain(int idTicket){
        trainDao.removeTicketFromTrain(idTicket);
   }
}