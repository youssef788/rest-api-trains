package com.example.api_rest.dao;

import com.example.api_rest.model.Ticket;
import com.example.api_rest.model.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ITrainDao extends JpaRepository<Train, Integer> {
    @Query("SELECT t from Train t where t.stationDeparture =:stationDeparture " +
            "AND t.stationArrival =:stationArrival " +
            "AND t.dateDeparture =:dateDeparture " +
            "AND t.dateArrival =:dateArrival " +
            "AND t.nbPlacesAvailable >=:nbPlacesAvailable")
    List<Train> findAvailableTrains(@Param("stationDeparture")String stationDeparture,
                                  @Param("stationArrival")String stationArrival,
                                  @Param("dateDeparture")Date dateDeparture,
                                  @Param("dateArrival")Date dateArrival,
                                  @Param("nbPlacesAvailable")Integer nbPlacesAvailable);

    @Query("SELECT t from Ticket t where t.idTrain =:idTrain " +
            "AND t.seatClass =:seatClass " +
            "AND t.flexible =:flexible ")
    List<Ticket> findCorrespondingTickets(@Param("idTrain")int idTrain,
                                          @Param("seatClass") String seatClass,
                                          @Param("flexible")boolean flexible);


    @Modifying
    @Query("DELETE from Ticket t where t.id =:idTicket ")
    void removeTicketFromTrain(@Param("idTicket")int idTicket);
}














