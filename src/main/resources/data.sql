DROP TABLE IF EXISTS train, ticket;

CREATE TABLE train (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    line VARCHAR(250) NOT NULL,
                    station_departure VARCHAR(250) NOT NULL,
                    station_arrival VARCHAR(250) NOT NULL,
                    date_departure DATETIME NOT NULL,
                    date_arrival DATETIME NOT NULL,
                    nb_places_available INT NOT NULL
);

CREATE TABLE ticket (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    seat_class VARCHAR(250) NOT NULL,
                    flexible BOOLEAN,
                    id_train INT,
                    foreign key (id_train) references train(id) on delete cascade

);

INSERT INTO train (line, station_departure, station_arrival, date_departure, date_arrival, nb_places_available)VALUES
                                    ('D line', 'Melun', 'Goussainville', {ts '2021-02-15 13:25:56'}, {ts '2021-02-15 15:43:23'}, '3'),
                                    ('TGV' ,'Paris Monparnasse', 'Rennes', {ts '2021-02-18 09:10:35'}, {ts '2021-02-18 10:30:12'}, '5'),
                                    ('TER' ,'Rambouillet', 'Invalides', {ts '2021-02-22 16:45:18'}, {ts '2021-02-22 17:55:48'}, '2')
                                    ;

INSERT  INTO ticket (seat_class, flexible, id_train) VALUES ('FIRST', true, 1),
                                                 ('STANDARD', true, 1),
                                                 ('STANDARD', false, 1),
                                                 ('FIRST', true, 2),
                                                 ('BUSINESS', true, 2),
                                                 ('BUSINESS', false, 2),
                                                 ('STANDARD', true, 2),
                                                 ('STANDARD', true, 2),
                                                 ('BUSINESS', false, 3),
                                                 ('BUSINESS', true, 3);

